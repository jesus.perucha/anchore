#!/usr/bin/env bash
docker build -t example-image:latest -f maven-java11-docker-aws-cli/Dockerfile .
curl -s https://ci-tools.anchore.io/inline_scan-v0.3.3 | bash -s -- -f -d maven-java11-docker-aws-cli/Dockerfile -b anchore-policies/policy_bundle_stop_high_no_fix.json example-image:latest
